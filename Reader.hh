<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\format\reader
{
	use nuclio\core\ClassManager;
	use nuclio\core\plugin\Plugin;
	use nuclio\plugin\format\driver\common\FormatException;
	use nuclio\plugin\format\driver\common\CommonInterface;
	use nuclio\plugin\provider\manager\Manager as ProviderManager;
	
	<<factory>>
	class Reader extends Plugin
	{
		public static function getContent(string $filename, Map<mixed,mixed> $options=Map{},?string $forceExt=null):mixed
		{
			$extension	=$forceExt ?? pathinfo($filename,PATHINFO_EXTENSION);
			$driver		=ProviderManager::request('format::'.$extension);
			
			if ($driver instanceof CommonInterface)
			{
				return $driver->read($filename,$options);
			}
			else
			{
				throw new FormatException(sprintf('No driver found for "%s" file type.',$extension));
			}
		}
	}
}